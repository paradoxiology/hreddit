
module Formatter (cleanupText,
                  wrapText,
                  wrapLine,
                  stripSuffix,
                  formatPluralNoun,
                  formatDiffTime,
                  parseRedditLinks,
                  between,
                  isBetween,
                  splitOn,
                  glob) where

import Data.List
import Data.Maybe
import Data.Time.Clock (NominalDiffTime)
import Text.Regex.Posix


replace :: Eq a => [a] -> [a] -> [a] -> [a]
replace _ _ [] = []
replace found repl s@(hs:ts) = case stripPrefix found s of
                                  Just ss  -> repl ++ replace found repl ss
                                  Nothing  -> hs : replace found repl ts


cleanupText :: String -> String
cleanupText = replace "\n\n" "\n" . replace "&gt;" ">" . replace "&lt;" "<" . replace "&amp;" "&"
                       

wrapText ::  Int -> String -> String
wrapText width text = unlines $ map (unlines . wrapLine width) (lines text)


wrapLine :: Int -> String -> [String]
wrapLine width = wrapLineWords width . words

                          
wrapLineWords :: Int -> [String] -> [String]
wrapLineWords _     [] = []
wrapLineWords width ws = 
  case wrapWordsAt width ws of
    (l, ls@(l':ls')) -> if null l then l' : wrapLineWords width ls' else unwords l : wrapLineWords width ls
    (l, [])          -> [unwords l]

oneSpaceWidth :: Int
oneSpaceWidth = 1

wrapWordsAt :: Int -> [String] -> ([String], [String])
wrapWordsAt _ [] = ([], [])
wrapWordsAt charLen ws'@(w:ws)
    | charLen' > 0 = let (lhs, rhs) = wrapWordsAt charLen' ws in
                     (w:lhs, rhs)
    | otherwise    = ([], ws')
        where charLen' = charLen - length w - oneSpaceWidth
                                         

stripSuffix :: Eq a => [a] -> [a] -> Maybe [a]                                         
stripSuffix ls s = case s `isSuffixOf` ls of
                       True  -> Just (take (length ls - length s) ls)
                       False -> Nothing

                                     
formatPluralNoun :: Integral i => i -> String -> String
formatPluralNoun i 
    | abs i > 1 = (++ "s") 
    | otherwise = id


formatPlural :: Show i => Integral i => i -> String -> String
formatPlural i unit 
    | i <= 1    = show (max 0 i) ++ " " ++ unit 
    | otherwise = show i ++ " " ++ unit ++ "s"


diffTimeUnits :: Integer -> [String]
diffTimeUnits diff = zipWith formatPlural (takeWhile (>0) diffTimesSizes) timeUnitNames
                     where timeUnitSizes  = [60, 60, 24, 30, 12]
                           timeUnitNames  = ["second", "minute", "hour", "day", "month", "year"]
                           diffTimesSizes = scanl div diff timeUnitSizes 

                            
formatDiffTime :: NominalDiffTime -> String
formatDiffTime = fromMaybe "0 second" . listToMaybe . reverse . diffTimeUnits . floor
-- 'fromMaybe "0 second" . listToMaybe' is just a safe way to get a list's head


parseRedditLinks :: String -> [(String, String)]
parseRedditLinks c = map parseMarkdownLink (concat $ c =~ pat)
  where pat = "\\[[^\\)]+\\]\\(http[s]?://[^\\)]*\\)"


parseMarkdownLink ::  String -> (String, String)
parseMarkdownLink = splitOn "](" . between "[" ")"


between ::  Eq a => [a] -> [a] -> [a] -> [a]
between l r = fst . splitOn r .snd . splitOn l 


isBetween ::  Eq a => [a] -> [a] -> [a] -> Bool
isBetween l r s = l `isPrefixOf` s && r `isSuffixOf` s


splitOn ::  Eq a => [a] -> [a] -> ([a], [a])
splitOn _ [] = ([], [])

splitOn d s@(h:t) = let mapFst f (x, y) = (f x, y) in
                        case stripPrefix d s of
                            Just r  -> ([], r) 
                            Nothing -> mapFst (h:) (splitOn d t)


-- Horribly inefficient naive simple glob implementation!
glob ::  [Char] -> [Char] -> Bool
glob [] [] = True

glob "*" (_:_) = True

glob [] _  = False

glob (a:b) (c:d) | a == c = glob b d

glob ('*':'*':b) s = glob ('*':b) s

glob p@('*':b) s@(_:d) = case span (/='*') b of
                             (_, [])  -> glob (reverse p) (reverse s)
                             (p', pr) -> case stripPrefix p' s of
                                             Just s' -> glob pr s'
                                             Nothing -> glob p d

glob _ _ = False


