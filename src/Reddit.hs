
module Reddit(redditDomain,
              RedditItem(..),
              ArticleIndex,
              ArticleListing(..),
              updateArticleListingIndices,
              Article(..),
              CommentListing(..),
              Comment(..),
              commentScore,
              NestedLevel,
              serialiseComments,
              UserInfo(..)) where

import Formatter

import Text.JSON
import Data.Maybe
import Data.Time
import System.Locale

import Network.URI (escapeURIString, isUnescapedInURI)
import Codec.Binary.UTF8.String (encodeString)

redditDomain :: String
redditDomain = "http://www.reddit.com"


class RedditItem i where
    itemID :: i -> String


type ArticleIndex = Int

data ArticleListing = ArticleListing 
  {
    listing    :: ![(ArticleIndex, Article)],
    pageAnchor :: !String
  } deriving Show


updateArticleListingIndices :: ArticleListing -> ArticleListing -> ArticleListing
updateArticleListingIndices (ArticleListing [] _) al = al

updateArticleListingIndices (ArticleListing ps _) al@(ArticleListing{ listing = ls }) = 
  al{ listing = mapFst (lastIndex+) ls}
    where
      mapFst f = map (\ (a, b) -> (f a, b))
      lastIndex  = (fst . last) ps

instance JSON ArticleListing where
  showJSON _ = undefined

  readJSON (JSObject obj) = do
    adata    <- valFromObj "data" obj
    children <- valFromObj "children" adata
    anchor   <- valFromObj "after" adata
    return $ ArticleListing (zip [1..] children) anchor

  readJSON _ = fail ""


data Article = Article 
 {
   articleID       :: !String,
   articleName     :: !String,
   articelSub      :: !String,
   articleAuthor   :: !String,
   articleTitle    :: !String,
   articleLink     :: !String,
   articleDomain   :: !String,
   articleURL      :: !String,
   articleSelfText :: !String,
   articleScore    :: !Int,
   articleDowns    :: !Int,
   articleUps      :: !Int,
   articleLiked    :: !(Maybe Bool),
   articleUTCTime  :: !UTCTime,
   articleComments :: !Int
 } deriving Show


parseRedditUTCTime :: Integer -> Result UTCTime
parseRedditUTCTime s =
  case parseTime defaultTimeLocale "%s" (show s) of
    Just utcTime -> Ok utcTime
    Nothing      -> Error $ "Failed to parse the UTC Time" ++ show s

escapeURL :: String -> String
escapeURL = escapeURIString isUnescapedInURI . encodeString

instance RedditItem Article where
  itemID = ("t3_" ++) . articleID  -- t3_ is required for the fullname id which means that the item is a reddit submission

                       
instance JSON Article where
  showJSON _ = undefined

  readJSON (JSObject obj) = 
    do
      adata     <- valFromObj "data" obj
      aid       <- valFromObj "id" adata
      name      <- valFromObj "name" adata
      subreddit <- valFromObj "subreddit" adata
      author    <- valFromObj "author" adata
      title     <- valFromObj "title" adata
      link      <- valFromObj "permalink" adata
      domain    <- valFromObj "domain" adata
      url       <- valFromObj "url" adata
      selftext  <- valFromObj "selftext" adata
      score     <- valFromObj "score" adata
      downs     <- valFromObj "downs" adata
      ups       <- valFromObj "ups" adata
      let liked = case (valFromObj "likes" adata) of
                    Ok (JSBool likes) -> Just likes
                    _                 -> Nothing
      utcTime          <- valFromObj "created_utc" adata >>= parseRedditUTCTime
      a_comments <- valFromObj "num_comments" adata
      return $ Article aid name subreddit author (cleanupText title) (escapeURL $ redditDomain ++ link) domain (escapeURL url) (cleanupText selftext) score downs ups liked utcTime a_comments

  readJSON _ = fail ""
    

data CommentListing = CommentListing 
  {
    commentedArticle :: !Article,
    comments :: ![Comment] 
  } deriving Show

instance JSON CommentListing where
    showJSON _ = undefined
        
    readJSON (JSArray (JSObject as : JSObject cs : _)) = do
                                   articles <- valFromObj "data" as >>= valFromObj "children"
                                   article  <- readJSON (fromMaybe JSNull $ listToMaybe articles)
                                   a_comments <- valFromObj "data" cs >>= valFromObj "children"
                                   return $ CommentListing article a_comments

    readJSON _ = fail ""


data Comment = Comment
 {
   commentBody    :: !String,
   commentAuthor  :: !String,
   commentUTCTime :: !UTCTime,
   commentUps     :: !Int,
   commentReplies :: ![Comment],
   commentLinks   :: [(String ,String)]
 }
 | More deriving Show


commentScore :: Comment -> Int
commentScore comment = commentUps comment


type NestedLevel = Int

serialiseComments :: NestedLevel -> [Comment] -> [(NestedLevel, Comment)]
serialiseComments _         []   = []
serialiseComments nestLevel (More:cs) = 
  serialiseComments nestLevel cs
serialiseComments nestLevel (c:cs) =
  (nestLevel, c) : serialiseComments (nestLevel + 1) (commentReplies c) ++ serialiseComments nestLevel cs

instance JSON Comment where
    showJSON _ = undefined
    
    readJSON (JSObject obj) = do
      kind   <- valFromObj "kind" obj
      if kind == "more"
          then return More
          else do
            adata   <- valFromObj "data" obj
            body    <- valFromObj "body" adata
            author  <- valFromObj "author" adata
            utcTime <- valFromObj "created_utc" adata >>= parseRedditUTCTime
            ups     <- valFromObj "ups" adata
            replies <- case valFromObj "replies" adata of
                         Ok (JSObject repliesObj) -> valFromObj "data" repliesObj >>= valFromObj "children"
                         _                        -> return []
            return $ Comment (cleanupText body) author utcTime ups replies (parseRedditLinks body)
                                  
    readJSON _ = fail ""


data UserInfo = UserInfo 
  {
    userName       :: !String,
    modHash        :: !String,
    subscribedSubs :: [String], -- Subscribed subreddits
    subscribedMuls :: [String]  -- Subscribed multireddits
  } deriving Show


instance JSON UserInfo where
    showJSON _ = undefined

    readJSON (JSObject obj) = do
      adata   <- valFromObj "data" obj
      modhash <- valFromObj "modhash" adata
      subdata <- valFromObj "children" adata
      subs    <- sequence $ map (\ r -> valFromObj "data" r >>= valFromObj "display_name") subdata
      return $ UserInfo "" modhash subs []

    readJSON _ = fail ""


