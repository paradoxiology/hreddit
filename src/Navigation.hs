{-# LANGUAGE FlexibleContexts, FlexibleInstances, TypeSynonymInstances, ScopedTypeVariables #-}

module Navigation (startReddit) where

import Reddit
import Renderer
import RedditActions
import Formatter(isBetween, between, glob)

import Data.Char (isSpace, isDigit, isUpper, toLower)
import Data.List (intercalate, isPrefixOf)
import Data.Maybe (fromMaybe, listToMaybe)
import Data.Time (getCurrentTime)

import Network.HTTP (HandleStream, HeaderName(..), getRequest, mkHeader, postRequest, rqHeaders, rspBody, rqBody)
import Network.Browser hiding (err)

import System.Console.ANSI
import System.Console.Haskeline hiding (catch)
import System.Directory (getTemporaryDirectory)
import System.FilePath ((</>))
import System.Info
import System.Process (system)

import Text.PrettyPrint.ANSI.Leijen (putDoc)

import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Error
import Control.Exception (catch)


data RedditPage = Subreddit !String
                | Multireddit !String
                | SearchResults !String

data ReadingState = ReadingListing !ArticleListing !RedditPage
                  | ReadingComments  ![(NestedLevel, Comment)] ![(NestedLevel, Comment)] !Article -- current and remaining nested Comments and the commented article
                  | NoState

data Command = GotoSubreddit !String
             | GotoArticle !ArticleIndex
             | GotoMutireddit !String
             | OpenArticleURL !ArticleIndex
             | OpenArticleDetails !ArticleIndex
             | OpenCommentURL !String
             | UpvoteArticle !ArticleIndex
             | DownvoteArticle !ArticleIndex
             | UnvoteArticle !ArticleIndex
             | Search !String
             | NextPage
             | GoBack
             | GoForward
             | Login !String
             | NoCommand
             | Exit


data NavigationState = NavigationState
  {
    curRState   :: !ReadingState,
    backList    :: ![ReadingState],
    forwardList :: ![ReadingState],
    userInfo    :: !(Maybe UserInfo),
    cookies     :: ![Cookie]
  }

initialNavState :: NavigationState
initialNavState = NavigationState NoState [] [] Nothing []


appendStateToHistory :: ReadingState -> NavigationState -> NavigationState
appendStateToHistory ns nav@(NavigationState{ curRState = cs, backList = bls }) =
  nav{ curRState = ns, backList = cs : (take 5 bls), forwardList = [] }


modifyCurrentState :: ReadingState -> NavigationState -> NavigationState
modifyCurrentState ns nav = nav{ curRState = ns }


navigateBack :: NavigationState -> NavigationState
navigateBack nav@(NavigationState{ backList = [] }) = nav

navigateBack nav@(NavigationState{ curRState = cs,
                                   backList = ps : bs,
                                   forwardList = fls})
                                    | null bs   = nav
                                    | otherwise = nav{ curRState = ps,
                                                       backList = bs,
                                                       forwardList = cs : fls}


navigateForward :: NavigationState -> NavigationState
navigateForward nav@(NavigationState{ forwardList = [] }) = nav

navigateForward nav@(NavigationState{ curRState = cs,
                                      backList = bls,
                                      forwardList = f: fs}) = nav{ curRState = f,
                                                                   backList = cs : bls,
                                                                   forwardList = fs}

updateNavCookies :: [Cookie] -> NavigationState -> NavigationState
updateNavCookies cs nav = nav{ cookies = cs }


type BrowserActionS = BrowserAction (HandleStream String)


-- The R monad that manages state recording, configuration retrieving and error handling
type R = ErrorT String (StateT NavigationState BrowserActionS)


liftBrowser :: BrowserActionS a -> R a
liftBrowser = lift . lift


instance Requestable R where
    requestURL url = do
      (uri, rsp) <- liftBrowser (request $ getRequest url)
      return (uri, rspBody rsp)

    postRequestURL url body = do
      let rqt0 = postRequest url
      let rqt1 = rqt0 { rqHeaders = mkHeader HdrContentType "application/x-www-form-urlencoded; charset=UTF-8" : [mkHeader HdrContentLength (show $ length body)] }
      let rqt = rqt1{ rqBody = body }
      (uri, rsp) <- liftBrowser (request rqt)
      cs <- liftBrowser getCookies
      modify $ updateNavCookies cs
      return $ (uri, rspBody rsp)


startReddit :: IO ()
startReddit = do
  setTitle "reddit: the front page of the internet"
  runReddit (GotoSubreddit "") initialNavState


redditBrowserAction cs = setupCookieFilter >> supressBrowserLogging >> setCookies cs where
                                setupCookieFilter = setCookieFilter f
                                                      where f _ c --  Cookie 'reddit_first' signals the user is the first time visitor, but it may interfere with the login session cookie
                                                             | ckName c == "reddit_first" = return False
                                                             | otherwise = return True


                                supressBrowserLogging = setOutHandler (const $ return ())


runReddit :: Command -> NavigationState -> IO ()
runReddit command navState =
  run command
  `catch` (\ err -> do
     putIOErr err
     run NoCommand)
  where
    run cmd = do
      r <- browse $ redditBrowserAction (cookies navState) >> runStateT (runErrorT (runCommand cmd)) navState
      case r of
        (Right Exit, _)              -> return ()
        (Right newCommand, newState) -> runReddit newCommand newState
        _                            -> return ()


parseCommand :: MonadError String m => String -> m Command
parseCommand s = let (_:as) = words s
                     argl   = unwords as in
                         case words s of
                             "r":subreddit:_ -> return $ GotoSubreddit subreddit
                             "r":[]          -> return $ GotoSubreddit ""
                             "m":multi:_     -> return $ GotoMutireddit multi
                             "m":_           -> throwError "Multireddit name is missing"
                             "g":index:_     -> apply GotoArticle index
                             "open":[]       -> return $ OpenArticleURL 0
                             "open":arg:_ | isBetween "[" "]" argl -> return $ OpenCommentURL (between "[" "]" argl)
                                          | all isDigit arg        -> return $ OpenArticleURL (fromMaybe 0 (maybeRead arg))
                                          | otherwise              -> throwError "Invalid article index"
                             "a":index:_     -> return $ OpenArticleDetails (fromMaybe 0 (maybeRead index))
                             "a":[]          -> return $ OpenArticleDetails 0
                             "upvote":ind:_  -> apply UpvoteArticle ind
                             "upvote":[]     -> return $ UpvoteArticle 0
                             "downvote":i:_  -> apply DownvoteArticle i
                             "downvote":[]   -> return $ DownvoteArticle 0
                             "unvote":ind:_  -> apply UnvoteArticle ind
                             "unvote":[]     -> return $ UnvoteArticle 0
                             "back":_        -> return GoBack
                             "forward":_     -> return GoForward
                             "search":query  -> return $ Search (intercalate "+"  query)
                             []              -> return NextPage
                             "login":u:_     -> return $ Login u
                             "login":_       -> throwError "Incomplete login command"
                             "exit":_        -> return Exit
                             _               -> throwError "Unknown command"
                             where maybeRead ""  = Just 0
                                   maybeRead str = case reads str of
                                                    [(x, trailing)] | all isSpace trailing -> Just x
                                                    _                                      -> Nothing

                                   apply cmd index = maybe (throwError "Invalid article index") (return . cmd) (maybeRead index)

commandList :: [String]
commandList = ["open", "upvote", "downvote", "unvote", "back", "forward", "search", "login", "exit"]


-- Popular subreddits
defaultSubs :: [String]
defaultSubs = ["4chan", "adviceanimals", "android", "apple", "art", "askreddit", "askscience", "atheism", "aww", "beer", "bestof", "blog", "books", "business", "canada", "circlejerk", "coding", "cogsci", "comics", "conspiracy", "cooking", "creepy", "design", "diy", "doesanybodyelse", "earthporn", "economics", "entertainment", "environment", "explainlikeimfive", "fffffffuuuuuuuuuuuu", "firstworldproblems", "fitness", "food", "frugal", "funny", "gadgets", "gaming", "gamedev", "geek", "gifs", "gonewild", "guns", "happy", "haskell", "health", "history", "howto", "humor", "iama", "itookapicture", "kindle", "lgbt", "libertarian", "linux", "listentothis", "lolcats", "loseit", "malefashionadvice", "math", "minecraft", "movies", "music", "netsec", "news", "nsfw", "offbeat", "philosophy", "photography", "physics", "pics", "politics", "programming", "psychology", "reddit.com", "science", "scifi", "seduction", "self", "sex", "shutupandtakemymoney", "skeptic", "space", "sports", "starcraft", "technology", "tf2", "tldr", "todayilearned", "trees", "truereddit",  "truegamedev", "twoxchromosomes", "videos", "web_design", "webgames", "wikipedia", "woahdude", "worldnews", "worldpolitics", "wtf", "youshouldknow"]


autocompleteLine :: ([String], [String]) -> String -> String -> [Completion]
autocompleteLine (subs, multis) prev cur
  | prev == ""   = completer cur commandList
  | prev == " r" = smartCompleter cur subs
  | prev == " m" = smartCompleter cur multis
  | otherwise    = []
    where
      isInsensitivePrefixOf pre str = (map toLower pre) `isPrefixOf` (map toLower str)
      insensitiveCompletion comp = Completion (map toLower comp) comp True
      -- Case-insensitive completer
      icompleter str = map insensitiveCompletion . filter (str `isInsensitivePrefixOf`)
      -- Case-sensitive completer
      completer str = map simpleCompletion . filter (str `isPrefixOf`)
      -- Use case-insensitive completer if the word contains any upper case
      -- character, otherwise case-sensitive completer will be used
      smartCompleter str =
        if any isUpper str
          then completer str
          else icompleter str


waitForInputStr :: (MonadIO m, MonadException m) => (Maybe UserInfo, ReadingState) -> InputT m String
waitForInputStr (user, rState) =
  do
    liftIO $ setCursorColumn 0
    cmdStr <- getInputLine $ userPrompt ++ contextPrompt
    return $ fromMaybe "" cmdStr
  where contextPrompt = case rState of
          ReadingListing _ (Subreddit "")  -> defaultPrompt
          ReadingListing _ (Subreddit sub) -> sub ++ "> "
          ReadingListing _ (Multireddit multi) -> multi ++ "> "
          ReadingListing _ (SearchResults _) -> "results.search> "
          ReadingComments  _ _ a -> "comments." ++ articelSub a ++ "> "
          NoState                -> defaultPrompt
          where defaultPrompt = "reddit> "
        userPrompt = case user of
          Just u  -> (userName u) ++ "@"
          Nothing -> ""


inputSettings :: MonadIO m => Maybe FilePath -> Maybe UserInfo -> Settings m
inputSettings histPath user = Settings
  {
    historyFile = histPath,
    complete = completeWordWithPrev Nothing " \t" completer,
    autoAddHistory = True
  }
    where
      compelteSubs = defaultSubs ++ (maybe [] subscribedSubs user)
      completeMutis = maybe [] subscribedMuls user
      completer prev cur = return $ autocompleteLine (compelteSubs, completeMutis) prev cur


passwordInputSettings :: MonadIO m => Settings m
passwordInputSettings = inputSettings Nothing Nothing

getUserInput :: Maybe UserInfo -> ReadingState -> IO String
getUserInput user readingState = do
  historyPath <- fmap (</> ".hreddit_history") getTemporaryDirectory
  runInputT (inputSettings (Just historyPath) user) (waitForInputStr (user, readingState))

getUserPassword :: String -> IO String
getUserPassword user =
  fmap (fromMaybe "") $
  runInputT passwordInputSettings (getPassword (Just '*') ("password for " ++ user ++ ": "))


runCommand :: Command -> R Command
runCommand Exit    = return Exit;
runCommand command =
  do
    execCommand command
    readingState <- gets curRState
    user <- gets userInfo
    cmdStr <- liftIO $ getUserInput user readingState
    parseCommand cmdStr
   `catchError` ( \err -> do
                          liftIO $ putErr err
                          runCommand NoCommand)

execCommand :: Command -> R ()
execCommand (GotoSubreddit subreddit) =
  do
    articleList <- downloadSubreddit subreddit ""
    modify (appendStateToHistory $ ReadingListing articleList (Subreddit subreddit))
    liftIO $ displayArticles (listing articleList)

execCommand (GotoMutireddit multi) =
  do
    user <- gets userInfo
    case user of
      Just _ -> do
        articleList <- downloadMultireddit multi ""
        modify (appendStateToHistory $ ReadingListing articleList (Multireddit multi))
        liftIO $ displayArticles (listing articleList)
      Nothing -> throwError "Multireddits is only available after login"

execCommand (Search query) =
  do
    articleList <- downloadSearchResults query ""
    modify (appendStateToHistory $ ReadingListing articleList (SearchResults query))
    liftIO $ displayArticles (listing articleList)


execCommand (GotoArticle index) =
  do
    readingState <- gets curRState
    case readingState of
      ReadingListing _ _ -> lookupArticle index (\ a ->
        do
          coms <- downloadComments (articleLink a)
          liftIO $ displayDetailArticle a
          let (ccs, ncs) = splitAt 4 (serialiseComments 0 coms)
          liftIO $ displayComments ccs
          modify (appendStateToHistory $ ReadingComments ccs ncs a) )
      _ -> throwError "Can't go to any article from here"

execCommand (OpenArticleURL index)  = lookupArticle index (startBrowser . articleURL)
                                       
execCommand (OpenArticleDetails index)  = lookupArticle index (liftIO . displayDetailArticle)


execCommand (OpenCommentURL globPat) = do
                                           readingState <- gets curRState
                                           case readingState of
                                               ReadingComments cs _ _ -> do
                                                 let err = "Can't find any link in the current comment page that matches the given glob pattern."
                                                 url <- lookupFM (glob globPat) err (concat $ map (commentLinks . snd) cs)
                                                 startBrowser url
                                               _                     -> throwError "Can't open comment link from here"



execCommand (UpvoteArticle index) = voteForArticle Upvote index

execCommand (DownvoteArticle index) = voteForArticle Downvote index

execCommand (UnvoteArticle index) = voteForArticle Neutral index


execCommand NextPage = do
  readingState <- gets curRState
  case readingState of
       ReadingComments _ cs a -> do
         let (ccs, ncs) = splitAt 6 cs
         liftIO $ displayComments ccs
         modify (modifyCurrentState $ ReadingComments ccs ncs a)
       ReadingListing articleList rp -> do
         nextPage <- fmap (updateArticleListingIndices articleList) $ downloadNextPage articleList rp
         modify (appendStateToHistory $ ReadingListing nextPage rp)
         liftIO $ displayArticles (listing nextPage)
           where
             downloadNextPage al (Subreddit sub) = downloadSubreddit sub (pageAnchor al)
             downloadNextPage al (Multireddit multi) = downloadMultireddit multi (pageAnchor al)
             downloadNextPage al (SearchResults kwords) = downloadSearchResults kwords (pageAnchor al)
                             
execCommand GoBack = do
  navState <- get
  let prevNavState = navigateBack navState
  put prevNavState
  liftIO $ displayContent (curRState prevNavState)
                              
execCommand GoForward =
  do
    navState <- get
    let nextNavState = navigateForward navState
    put nextNavState
    liftIO $ displayContent (curRState nextNavState)

execCommand (Login username) =
  do
    passwd <- liftIO $ getUserPassword username
    result <- loginReddit username passwd
    case result of
        Just user -> do 
          liftIO (putStrLn $ "Successfully logged in!\tWelcome back " ++ userName user ++ "!")
          modify (\ navState -> navState{ userInfo = Just user })
          rState <- gets curRState
          case rState of
            ReadingListing _ _ -> execCommand (GotoSubreddit "")
            _                  -> return ()

        Nothing       -> modify (\ navState -> navState{ userInfo = Nothing }) >>
                         liftIO (putStrLn "Failed to login!")

execCommand NoCommand = return ()


startBrowser ::  MonadIO m => String -> m ()
startBrowser url = liftIO $ system (openCmd ++ "'" ++ url ++ "'") >> return ()
  where
    openCmd = case os of
      "linux"   -> "xdg-open "
      "darwin"  -> "open "
      "mingw32" -> "start "
      _         -> "start "


voteForArticle :: (Article -> VoteDirection Article)-> ArticleIndex-> R ()
voteForArticle voteDir index =
  do
    user <- gets userInfo
    lookupArticle index $ voteArticle user . voteDir


lookupArticle :: ArticleIndex -> (Article -> R a) -> R a
lookupArticle index f =
  do
    readingState <- gets curRState
    case readingState of
        ReadingComments _ _ a        -> f a
        ReadingListing articleList _ -> lookupM index "Invalid article number" (listing articleList) >>= f
        _                            -> throwError "Error"


lookupM :: (Monad m, Eq k) => k -> String -> [(k, e)] -> m e
lookupM k err ls = maybe (fail err) return (lookup k ls)


lookupFM :: (Monad m) => (a -> Bool) -> String -> [(a, b)] -> m b
lookupFM f err ls  = maybe (fail err) (return . snd) (listToMaybe $ filter (f . fst) ls)

displayContent :: ReadingState -> IO ()
displayContent (ReadingComments pcs _ _)   = displayComments pcs
displayContent (ReadingListing articleList _) = displayArticles (listing articleList)
displayContent NoState = return ()


displayArticles :: [(Int, Article)] -> IO ()
displayArticles articles = do
  cUTC <- getCurrentTime
  putStrLn (replicate 80 '*')
  mapM_ (putDoc . articleToDoc cUTC) articles
  `catch` putIOErr


displayDetailArticle :: Article -> IO ()
displayDetailArticle article = do
  cUTC <- getCurrentTime
  putStrLn (replicate 80 '*')
  putDoc $ detailArticleToDoc cUTC article
  `catch` putIOErr


displayComments :: [(NestedLevel, Comment)] -> IO ()
displayComments [] = return ()
displayComments cs = do
  cUTC <- getCurrentTime
  cursorUpLine 1 >> clearLine -- Clear the command prompt e.g. reddit>_
  putStrLn (replicate 80 '-') -- Display the page delimiter
  putDoc $ commentsToDoc cUTC cs
  `catch` putIOErr


putErr :: String -> IO ()
putErr errMsg = setSGR [SetColor Foreground Vivid Red] >> putStrLn errMsg >> setSGR [Reset]

putIOErr :: IOException -> IO ()
putIOErr ex = putErr (show ex)

