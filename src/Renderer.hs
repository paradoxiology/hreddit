
module Renderer (articleToDoc, detailArticleToDoc, commentsToDoc) where

import Reddit
import Formatter

import Data.Time.Clock (diffUTCTime, UTCTime)
import Text.PrettyPrint.ANSI.Leijen


wordWrapLineLen :: Int
wordWrapLineLen = 80

scoreColumnLen :: Int
scoreColumnLen = 10


articleMetaInfo :: UTCTime -> Article -> Doc
articleMetaInfo cUTC a =  bold (int (articleComments a) <+> (text "comments")) <>
                          text "\tsubmitted" <+>
                          text (formatDiffTime $ diffUTCTime cUTC (articleUTCTime a)) <+>
                          text "ago by" <+> dullcyan (text $ articleAuthor a) <+>
                          text "to" <+> dullcyan (text $ articelSub a)


titleDomainLinesToDocLines :: [String] -> String -> [Doc]
titleDomainLinesToDocLines []         _ = []
titleDomainLinesToDocLines [lastLine] d =
  case stripSuffix lastLine d of
   Just lastTitleLine -> [text lastTitleLine <+> black (text d)]
   Nothing            -> [text lastLine]
titleDomainLinesToDocLines (l:ls)     d = text l : titleDomainLinesToDocLines ls d


articleTitleToDocLines :: Article -> [Doc]
articleTitleToDocLines a =
  titleDomainLinesToDocLines (wrapLine wordWrapLineLen (articleTitle a ++ domain)) domain
    where domain = '(' : articleDomain a ++ ")"

articleToDoc :: UTCTime -> (Int, Article) -> Doc
articleToDoc cUTC (i, a) =
  hcat (map rowToDoc rows) <> indent scoreColumnLen (black $ articleMetaInfo cUTC a) <$$> linebreak
    where
      titleDocLines      = articleTitleToDocLines a
      likedColour liked  = if liked then dullmagenta else dullgreen
      scoreColour        = maybe id likedColour (articleLiked a)
      rows               = zip ((dullred (int i <> dot) <+> scoreColour (int $ articleScore a)) : repeat space) titleDocLines
      rowToDoc (c0, c1)  = fill scoreColumnLen c0 <> c1 <> linebreak

detailArticleToDoc :: UTCTime -> Article -> Doc
detailArticleToDoc cUTC a =
  hcat (map rowToDoc rows) <> indent scoreColumnLen (delimiter <$$> selfTextDocLines <$$> black (articleMetaInfo cUTC a)) <$$> linebreak
    where
      titleDocLines     = articleTitleToDocLines a
      scoreDoc          = dullmagenta (int $ articleScore a) <+> text "up votes"
      delimiter         = black $ text (replicate 60 '#')
      rows              = zip (replicate (length titleDocLines) space ++ [white (int $ articleScore a)] ++ repeat space) (titleDocLines ++ [scoreDoc])
      rowToDoc (c0, c1) = fill scoreColumnLen c0 <> c1 <> linebreak
      selfTextDocLines  = vcat (map text (lines $ wrapText wordWrapLineLen (articleSelfText a)))


commentToDoc :: UTCTime -> NestedLevel -> Comment -> Doc
commentToDoc _    _         More = empty
commentToDoc cUTC nestLevel c =
  indent (nestLevel * 2 + 1) (authorInfoDoc c <$$> bodyLinesDoc c)
    where
      score = commentScore c
      authorInfoDoc More = dullcyan (text "more")
      authorInfoDoc com  = dullcyan (text $ commentAuthor com) <+>
                           dullmagenta (int score <+>
                           text (formatPluralNoun score "point")) <+>
                           black (text (formatDiffTime $ diffUTCTime cUTC (commentUTCTime com)) <+> text "ago")

      bodyLinesDoc More = empty
      bodyLinesDoc com  = vcat (map text (lines $ wrapText (wordWrapLineLen - nestLevel) (commentBody com)))


commentsToDoc :: UTCTime -> [(NestedLevel, Comment)] -> Doc
commentsToDoc cUTC cs = vcat $ map (uncurry $ commentToDoc cUTC) cs


