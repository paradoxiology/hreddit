{-# LANGUAGE FlexibleContexts #-}

module RedditActions
  (URL,
   Requestable(..),
   loginReddit,
   downloadSubreddit,
   downloadMultireddit,
   downloadSearchResults,
   downloadComments,
   VoteDirection(..),
   voteArticle)
  where

import Reddit
import Text.JSON

import Network.URI (URI)

import Control.Monad.Error (MonadError, throwError)
import Control.Monad.IO.Class


type URL = String

class MonadIO r => Requestable r where
    requestURL :: URL -> r (URI, String)
    
    postRequestURL :: URL -> String -> r (URI, String)
    
    
loginReddit :: Requestable m => String -> String -> m (Maybe UserInfo)
loginReddit user passwd =
  do
    (_, _) <- postRequestURL loginPostURL loginPostBdy
    (_, rspt) <- requestURL userInfoURL
    case decode rspt of
      Ok userInfo -> do
        muti <- retrieveMutlreddits
        return . Just $ userInfo{ userName = user, subscribedMuls = muti }
      Error _     -> return Nothing
  where
    loginPostBdy = "api_type=json&user=" ++ user ++ "&passwd=" ++ passwd
    loginPostURL = redditDomain ++ "/api/login/" ++ user
    userInfoURL = redditDomain ++ "/reddits/mine.json?limit=500"


-- Only after login
retrieveMutlreddits :: Requestable m => m [String]
retrieveMutlreddits = do
  (_, rspt) <- requestURL $ redditDomain ++ "/api/multi/mine.json"
  case decode rspt of
    Ok ls ->
      let vs = sequence $ map (\j -> valFromObj "data" j  >>= valFromObj "name") ls in
      case vs of
        Ok ms -> return ms
        _ -> return []
    _ -> return []

downloadSubreddit :: (MonadError String m, Requestable m) => String -> String -> m ArticleListing
downloadSubreddit subreddit anchor =
  downloadJSON url
    where url = if null subreddit
                  then redditDomain ++ "/.json?limit=12&after=" ++ anchor
                  else redditDomain ++ "/r/" ++ subreddit ++ "/.json?limit=12&after=" ++ anchor

downloadMultireddit :: (MonadError String m, Requestable m) => String -> String -> m ArticleListing
downloadMultireddit multi anchor =
  downloadJSON url
    where url = redditDomain ++ "/me/m/" ++ multi ++ "/.json?limit=12&after=" ++ anchor

downloadSearchResults :: (MonadError String m, Requestable m) => URL -> String -> m ArticleListing
downloadSearchResults query anchor =
  downloadJSON url
    where url = redditDomain ++ "/search.json?limit=12&q=" ++ query ++ "&after=" ++ anchor

downloadComments :: (MonadError String m, Requestable m) => URL -> m [Comment]
downloadComments url = downloadJSON (url ++ ".json?sort=best") >>= return . comments


downloadJSON :: (MonadError String m, Requestable m, JSON a) => URL -> m a
downloadJSON url =
  do
    (_, content) <- requestURL url
    case decode content of
      Ok result -> return result
      Error _   -> throwError "Not a valid Reddit page"

data VoteDirection a = Upvote a
                     | Downvote a
                     | Neutral  a deriving (Show, Eq)

voteArticle :: (Requestable m) => Maybe UserInfo -> VoteDirection Article -> m ()
voteArticle Nothing _ = return ()

voteArticle (Just u) vd =
    case vd of
      Upvote   a -> vote a "1"
      Downvote a -> vote a "-1"
      Neutral  a -> vote a "0"
    where vote a dir = postRequestURL (redditDomain ++ "/api/vote") (votePostBody dir u a) >> return ()


votePostBody :: RedditItem item => String -> UserInfo -> item -> String
votePostBody dir u item = "id=" ++ itemID item ++ "&uh=" ++ modHash u  ++ "&dir=" ++ dir

