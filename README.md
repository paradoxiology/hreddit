# A Command Line Reddit Viewer written in Haskell


## Features

* Browse reddit and any subreddits from within the terminal
* Pagination
* Search reddit
* Ability to login to your account to view the customised front page
* Ability to go forwards/backwards in the browsing history
* Ability to upvote, downvote, and unvote a Reddit submission

## How to use

For best viewing results, please make sure the terminal/command line window to be at least **80-character wide** and **50-character high**

* **r** : Go to the front page
* **r** _subreddit_ : Go to the specified subreddit
* **g** _submission_index_ : When in the list view, read the comments of the submission
* _enter/return key_ : Go to next page, applicable in both list and comment views
* **a** _submission_index_ : When in the list view, display the content text associated with the article (Relevant for text based reddit submissions)
* **a** : When in the comment view, display the text associated with the current submission
* **open** _submission_index_ : When in the list view, open the link associated with the submission
* **open** : When in the comment view, open the URL associated with the current submission
* **open** [_glob_patttern_]: When in the comment view, open the URL associated with the link that matches the glob pattern. _Note: only the simple glob wildcard: * is supported at the moment_
* **search** _keywords_ : Search reddit with the specified keywords
* **login** _username_ : Login; will be prompted for password
* **back** : Go back to the last page/view
* **forward** : Go forwards one page
* **upvote** _submission_index_ : upvote this submission, only applicable if logged in.
* **downvote** _submission_index_ : downvote this submission, only applicable if logged in.
* **unvote** _submission_index_ : Remove vote from this submission, only applicable if logged in.
* **exit** : What it says

## New in v1.3.2
* Updated cabal file so it could be compiled against Haskell Platform 2013

## New in v1.3.1
+ Finally fixed the dreaded http connection issue, no more disconnection after not using it for a few minutes.

## New in v1.3
+ Added ability to open links in the comment view, use command `open [glob_pattern]` to open the URL associated with the link


## New in v1.2
+ Added ability to vote a Reddit submission: upvote, unvote, downvote


## Compile Notes
Developed on the Haskell Platform 2013,2.0.0, the viewer can be compiled using ghc v7.6.3 with the following 3rd-party packages/libraries(Other versions/platforms of the compiler should do too, but I haven't gotten around to testing them yet)

* json for Text.JSON
* ansi-terminal for System.Console.ANSI
* ansi-wl-pprint for Text.PrettyPrint.ANSI.Leijen



## License
The source is released under the [MIT](http://www.opensource.org/licenses/mit-license.php) license.
